import { Figure } from "./figure";
import { FigureFactory } from "./figureFactory";
import { StandartFigureFactory } from "./standartFigureFactory";

abstract class Observer {
    protected _source: Source

    constructor(s: Source) {
        this._source = s;
    }

    public abstract update(f: Figure): void;
    public abstract sub(s: Source): void;
    public abstract unsub(): void;
}

class TetrisObserver extends Observer {
    private _id: number;
    private static _idCounter: number = 0;

    constructor(s: Source) {
        super(s);
        this._id = ++TetrisObserver._idCounter;
        this._source = null;
    }

    public sub(s: Source): void {
        if (!this._source) {
            this._source = s;
            s.add(this);
        }
    }

    public update(f: Figure): void {
        if (this._source) {
            console.log('Observer #' + this._id + ': heard the figure:' + f.toString());
        }
    }

    public unsub(): void {
        if (this._source) {
            this._source.remove(this);
            this._source = null;
        }
    }
}

abstract class Source {
    protected _observers: Observer[];

    constructor() {
        this._observers = [];
    }

    public abstract notify(): void;
    public abstract add(o: Observer): void;
    public abstract remove(o: Observer): void;
}

class TetrisSource extends Source {
    private _factory: FigureFactory;

    constructor(f: FigureFactory) {
        super();
        this._factory = f;
    }

    public notify(): void {
        const figure: Figure = this._factory.getFigure();
        this._observers.forEach((o) => {
            o.update(figure);
        });
    }

    public add(o: Observer): void {
        if (!this._observers.includes(o)) {
            this._observers.push(o);
            o.sub(this);
        }
    }

    public remove(o: Observer): void {
        if (this._observers.includes(o)) {
            this._observers.splice(this._observers.indexOf(o), 1);
            o.unsub();
        }
    }
}