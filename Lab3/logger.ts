export class Logger {
    private static _instance: Logger = new Logger();
    
    private constructor() {

    }

    public static getInstance(): Logger {
        return Logger._instance;
    }
    
    public log(s:string):void {
        console.log(new Date() + ': ' + s);
    }
}