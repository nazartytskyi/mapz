import { Logger } from './logger';
import { Score } from "./score";
import { Block } from "./block";
import { Figure } from './figure';
import { FigureFactory } from "./figureFactory";
import { TetrisMemento } from "./tetrisMemento";
import { TetrisStrategy } from "./tetrisStrategy";

export class TetrisGame {
    private _logger: Logger;
    private _score: Score;
    private _gravity: number;
    private _blocks: Block[][];
    private _nextFigure: Figure;
    private _running: boolean;
    private _factory: FigureFactory;
    private _r: number;
    private check: Function;

    constructor(strategy: TetrisStrategy) {
        this._logger = Logger.getInstance();
        this._score = Score.getInstance();
        this._score.score = 0;
        this._running = false;
        this._blocks = [];
        this._factory = strategy.getFactory();
        this._gravity = strategy.getGravity();
        this._nextFigure = this._factory.getFigure();
        this.check = strategy.check;
    }

    public run() {
        this._r = setInterval(() => {
            this._running = true;
            this._nextFigure = this._factory.getFigure();
            this._score.modify(Math.floor(Math.random() * 10));
            this.check();
        }, 2000)
    }

    public pause() {
        this._running = false;
        clearInterval(this._r);
    }

    public saveGame(): TetrisMemento {
        return new TetrisMemento(this._score.score, this._blocks);
    }

    public loadGame(memento: TetrisMemento): void {
        this._score.score = memento.score;
        this._blocks = memento.blocks;
        this._running = false;
    }
}