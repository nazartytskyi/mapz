import { Figure } from './figure';

export abstract class FigureFactory {
    public getFigure(): Figure {
        return new Figure();
    }
}