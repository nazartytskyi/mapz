import { FigureFactory } from './figureFactory';
import { StandartFigureFactory } from './standartFigureFactory';
import { StrangeFigureFactory } from "./strangeFigureFactory";

export abstract class TetrisStrategy {
    public abstract getGravity(): number;
    public abstract getFactory(): FigureFactory;
    public abstract check(): void;
}

export class EasyDifficultyStrategy extends TetrisStrategy {
    public getGravity(): number {
        return 10;
    }
    public getFactory(): FigureFactory {
        return new StandartFigureFactory();
    }
    public check(): void {
        console.log('Easy');
    }
}

export class MediumDifficultyStrategy extends TetrisStrategy {
    public getGravity(): number {
        return 20;
    }
    public getFactory(): FigureFactory {
        return new StandartFigureFactory();
    }
    public check(): void {
        console.log('Medium');
    }
}

export class HardDifficultyStrategy extends TetrisStrategy {
    public getGravity(): number {
        return 20;
    }
    public getFactory(): FigureFactory {
        return new StrangeFigureFactory();
    }
    public check(): void {
        console.log('Hard');
    }
}