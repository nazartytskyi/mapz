import { Block } from "./block";
import { Figure } from "./figure";

export class TetrisMemento {
    readonly score: number;
    readonly blocks: Block[][];

    constructor(score: number, blocks: Block[][]) {
        this.score = score;
        this.blocks = blocks;
    }
}