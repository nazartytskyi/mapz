export class Score {
    private static _instance: Score = null;
    private _score: number;
    
    private constructor() { 
        this._score = 0;
    }
    
    public static getInstance(): Score {
        if (Score._instance == null) {
            Score._instance = new Score();
        }
        return Score._instance;
    }
    
    public modify(diff: number):void {
        this._score += diff;
    }

    public get score(): number {
        return this._score;
    }

    public set score(v : number) {
        this._score = v;
    }
    
}