import { randomColor } from './color';
import { Block } from './block';
import { Figure } from './figure';
import { FigureFactory } from './figureFactory';

export enum StandartFigures {
    OFigure = 0,
    IFigure = 1,
    LFigure = 2,
    JFigure = 3,
    TFigure = 4,
    ZFigure = 5,
    SFigure = 6
};

export class StandartFigureFactory extends FigureFactory {
    private static _figuresCount = 7;

    public getFigure(): Figure {
        const figureNum: number = Math.floor(Math.random() * StandartFigureFactory._figuresCount);
        const color = randomColor();

        switch (figureNum) {
            case StandartFigures.OFigure:
                return new Figure([
                    [new Block(color), new Block(color)],
                    [new Block(color), new Block(color)]
                ]);
            case StandartFigures.IFigure:
                return new Figure([
                    [new Block(color)],
                    [new Block(color)],
                    [new Block(color)],
                    [new Block(color)]
                ]);
            case StandartFigures.LFigure:
                return new Figure([
                    [new Block(color)],
                    [new Block(color)],
                    [new Block(color), new Block(color)]
                ]);
            case StandartFigures.JFigure:
                return new Figure([
                    [null,              new Block(color)],
                    [null,              new Block(color)],
                    [new Block(color),  new Block(color)]
                ]);
            case StandartFigures.TFigure:
                return new Figure([
                    [new Block(color),  new Block(color), new Block(color)],
                    [null,              new Block(color)]
                ]);
            case StandartFigures.ZFigure:
                return new Figure([
                    [new Block(color), new Block(color)],
                    [null, new Block(color), new Block(color)]
                ]);
            case StandartFigures.SFigure:
                return new Figure([
                    [null, new Block(color), new Block(color)],
                    [new Block(color), new Block(color)]
                ]);
            default:
                throw 'unknown standart figure';
        }
    }
}