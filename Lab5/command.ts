interface Command {
    execute(): void;
}

class UpdateRatingCommand implements Command {
    private _receiver: Receiver;
    private _score: number;

    constructor(receiver: Receiver, score: number) {
        this._receiver = receiver;
        this._score = score;
    }

    public execute(): void {
        this._receiver.updateScore(this._score);
    }
}

class Receiver {
    public updateScore(score: number): void {
        console.log('score updated to ' + score);
    }
}

class Invoker {
    private _commands: Command[];

    constructor() {
        this._commands = [];
    }

    public addCommand(command: Command) {
        this._commands.push(command);
    }

    public perform(): void {
        this._commands.forEach((command) => {
            command.execute();
        })
        this._commands = [];
    }

    public performFirst(): void {
        if (this._commands.length > 0) {
            this._commands[0].execute();
            this._commands.shift();
        }
    }
}