import { Color } from './color';

export class Block {
    private color: Color;
    
    constructor(color: Color) {
        this.color = color;
    }
    
    public clone(): Block {
        return new Block(this.color);
    }
}