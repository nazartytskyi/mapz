import { TetrisMemento } from "./tetrisMemento";
import { TetrisGame } from "./tetrisGame";

export class TetrisCaretaker {
    private _game: TetrisGame;
    private _saves: TetrisMemento[];

    constructor(game: TetrisGame) {
        this._game = game;
    }

    public save(): void {
        this._saves.push(this._game.saveGame());
    }

    public load(): void {
        this._game.loadGame(this._saves.pop());
    }
}