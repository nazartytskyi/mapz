export class Color {
    static readonly lowColorBound: number = 0;
    static readonly upColorBound: number = 255;
    static readonly lowAlphaBound: number = 0;
    static readonly upAlphaBound: number = 1;

    readonly red: number;
    readonly green: number;
    readonly blue: number;
    readonly alpha: number;

    constructor(red: number, green: number, blue: number, alpha = 1) {
        if (red >= Color.lowColorBound && red <= Color.upColorBound) {
            this.red = red;
        } else {
            throw 'bad red ' + red;
        }

        if (green >= Color.lowColorBound && green <= Color.upColorBound) {
            this.green = green;
        } else {
            throw 'bad green ' + green;
        }

        if (blue >= Color.lowColorBound && blue <= Color.upColorBound) {
            this.blue = blue;
        } else {
            throw 'bad blue ' + blue;
        }

        if (alpha >= Color.lowAlphaBound && alpha <= Color.upAlphaBound) {
            this.alpha = alpha;
        } else {
            throw 'bad alpha ' + alpha;
        }
    }
}

export function randomColor(): Color {
    const range = Color.upColorBound - Color.lowColorBound + 1;
    const red = Math.floor(Math.random() * range + Color.lowColorBound);
    const green = Math.floor(Math.random() * range + Color.lowColorBound);
    const blue = Math.floor(Math.random() * range + Color.lowColorBound);
    return new Color(red, green, blue);
}