import { randomColor } from './color';
import { Block } from './block';
import { Figure } from './figure';
import { FigureFactory } from './figureFactory';

export class StrangeFigureFactory extends FigureFactory {
    private static _figuresCount = 7;

    public getFigure(): Figure {
        const figureNum: number = Math.floor(Math.random() * StrangeFigureFactory._figuresCount);
        const color = randomColor();

        switch (figureNum) {
            case 0:
                return new Figure([
                    [new Block(color)]
                ]);
            case 1:
                return new Figure([
                    [null, new Block(color)],
                    [new Block(color), new Block(color)],
                    [null, new Block(color)],
                    [null, new Block(color)]
                ]);
            case 2:
                return new Figure([
                    [new Block(color)],
                    [new Block(color), new Block(color)],
                    [new Block(color)],
                    [new Block(color)]
                ]);
            case 3:
                return new Figure([
                    [new Block(color), new Block(color), new Block(color)],
                    [new Block(color), null, new Block(color)]
                ]);
            case 4:
                return new Figure([
                    [new Block(color), new Block(color)],
                    [new Block(color), new Block(color)],
                    [null, new Block(color)]
                ]);
            case 5:
                return new Figure([
                    [new Block(color), new Block(color)],
                    [new Block(color), new Block(color)],
                    [new Block(color)]
                ]);
            case 6:
                return new Figure([
                    [null, new Block(color)],
                    [new Block(color), new Block(color), new Block(color)],
                    [new Block(color), null, new Block(color)]
                ]);
        }
    }
}