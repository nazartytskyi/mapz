import { Block } from './block';

export class Figure {
    private _blocks: Block[][];
    
    constructor(blocks: Block[][] = []) {
        this._blocks = blocks;
    }
    
    public clone(): Figure {
        const clBlocks: Block[][] = [];
        this._blocks.forEach((arr, i) => {
            clBlocks[i] = [];
            arr.forEach((bl, j) => {
                if (bl instanceof Block) {
                    clBlocks[i][j] = bl.clone();
                } else {
                    clBlocks[i][j] = null;
                }
            });
        });
        return new Figure(clBlocks);
    }
}