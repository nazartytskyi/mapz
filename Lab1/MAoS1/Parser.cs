﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    static class Parser
    {
        struct OperatorInfo
        {
            public int Priority { get; set; }
            public int ParamCount { get; set; }
            public Type Type { get; set; }
            public OperatorInfo(int prior, int parcnt, Type type)
            {
                Priority = prior;
                ParamCount = parcnt;
                Type = type;
            }
        }

        static readonly Dictionary<string, OperatorInfo> operators = new Dictionary<string, OperatorInfo>()
        {
            {"|",  new OperatorInfo(1, 2, typeof(OrExpression))},
            {"&",  new OperatorInfo(2, 2, typeof(AndExpression))},
            {"==", new OperatorInfo(3, 2, typeof(EqualExpression))},
            {"!=", new OperatorInfo(3, 2, typeof(NotEqualExpression))},
            {"<",  new OperatorInfo(4, 2, typeof(LessExpression))},
            {"<=", new OperatorInfo(4, 2, typeof(LessEqualExpression))},
            {">",  new OperatorInfo(4, 2, typeof(MoreExpression))},
            {">=", new OperatorInfo(4, 2, typeof(MoreEqualExpression))},
            {"+",  new OperatorInfo(5, 2, typeof(AdditionExpression))},
            {"-",  new OperatorInfo(5, 2, typeof(SubtractionExpression))},
            {"*",  new OperatorInfo(6, 2, typeof(MultiplicationExpression))},
            {"/",  new OperatorInfo(6, 2, typeof(DivisionExpression))},
            {"^",  new OperatorInfo(7, 2, typeof(PowerExpression))},
            {"!",  new OperatorInfo(8, 1, typeof(NotExpression))},
        };

        private static object[] SubArray(object[] src, int pos, int length = -1)
        {
            if (length < 0)
                length = src.Count() - pos;

            object[] sub = new object[length];
            Array.Copy(src, pos, sub, 0, length);
            return sub;
        }
        
        private static void ALUAddNode(Stack<AbstractExpression> nodeStack, Stack<string> opStack)
        {
            string nodeOp = opStack.Pop();
            OperatorInfo nodeOpInfo = operators[nodeOp];
            if (nodeStack.Count < nodeOpInfo.ParamCount)
                throw new Exception("Not enough operands");

            AbstractExpression lhs = null;
            if (nodeOpInfo.ParamCount == 1)
            {
                lhs = nodeStack.Pop();
                nodeStack.Push((AbstractExpression) Activator.CreateInstance(nodeOpInfo.Type, lhs));
            }
            else
            {
                AbstractExpression rhs = nodeStack.Pop();
                lhs = nodeStack.Pop();
                nodeStack.Push((AbstractExpression)Activator.CreateInstance(nodeOpInfo.Type, lhs, rhs));
            }
        }

        private static AbstractExpression ALUParse(object[] tokens)
        {
            Stack<AbstractExpression> nodeStack = new Stack<AbstractExpression>();
            Stack<string> opStack = new Stack<string>();
            
            foreach(var token in tokens)
            {
                if (token is ConstantType || token is VariableExpression)
                    nodeStack.Push((AbstractExpression)token);
                else if (token is string sToken)
                {
                    if (sToken == "(")
                    {
                        opStack.Push("(");
                        continue;
                    }

                    if (sToken == ")")
                    {
                        while (opStack.Count > 0 && opStack.Peek() != "(")
                            ALUAddNode(nodeStack, opStack);

                        if (opStack.Count == 0)
                            throw new Exception("Not enough opening brackets.");

                        opStack.Pop();
                        continue;
                    }

                    if (operators.ContainsKey(sToken))
                    {
                        while (opStack.Count > 0)
                        {
                            if (opStack.Peek() != "(" && operators[opStack.Peek()].Priority >= operators[sToken].Priority)
                                ALUAddNode(nodeStack, opStack);
                            else
                                break;
                        }
                        opStack.Push(sToken);
                        continue;
                    }
                    else
                        throw new Exception($"Invalid operator '{sToken}'.");
                }
                else
                    throw new Exception("Corrupted array of tokens.");
            }
            while (opStack.Count > 0)
            {
                if (opStack.Peek() == "(")
                    throw new Exception("Not enough opening brackets.");
                ALUAddNode(nodeStack, opStack);
            }

            if (nodeStack.Count > 1)
                throw new Exception("Too many operands.");

            return nodeStack.Pop();
        }

        private static AbstractExpression BuildAST(object[] tokens)
        {
            if (tokens.Count() == 0)
                throw new Exception("A void row was passed to Parser.");

            int assignIndex = Array.IndexOf(tokens, "=");
            if (assignIndex != -1)
            {
                return new AssignExpression(BuildAST(SubArray(tokens, 0, assignIndex)),
                                            BuildAST(SubArray(tokens, assignIndex + 1)));
            }

            if (tokens[0] is string str0)
            {
                switch(str0)
                {
                    case "var":
                        if (tokens[1] is VariableExpression)
                        {
                            return new VarExpression((VariableExpression)tokens[1]);
                        }
                        else throw new Exception("Invalid variable name");
                        
                    case "print":
                        return new PrintExpression(BuildAST(SubArray(tokens, 1)));
                        
                    case "GetBrowser":
                        return new GetBrowserExpression();

                    case "click":
                        return new ClickExpression(BuildAST(SubArray(tokens, 1)));

                    case "sleep":
                        return new SleepExpression(BuildAST(SubArray(tokens, 1)));

                    case "if":
                        if (tokens.Count() < 3)
                            throw new Exception("Invalid if statement.");
                        if ((string) tokens[tokens.Count() - 2] == "else")
                        {
                            if (tokens.Count() < 5)
                                throw new Exception("Invalid if..else statement.");
                            return new IfExpression(BuildAST(SubArray(tokens, 1, tokens.Count() - 4)),
                                                    (string) tokens[tokens.Count() - 3], 
                                                    (string) tokens[tokens.Count() - 1]);
                        }
                        return new IfExpression(BuildAST(SubArray(tokens, 1, tokens.Count() - 2)),
                                                    (string)tokens[tokens.Count() - 1]);

                    case "while":
                        if (tokens.Count() < 3)
                            throw new Exception("Invalid while statement.");
                        return new WhileExpression(BuildAST(SubArray(tokens, 1, tokens.Count() - 2)),
                                                    (string)tokens[tokens.Count() - 1]);


                }
            }

            if (tokens.Count() == 1)
            {
                if (tokens[0] is ConstantType || tokens[0] is VariableExpression)
                    return (AbstractExpression)tokens[0];
            }

            if (tokens.Count() > 1 && tokens[1] is string str1)
            {
                switch (str1)
                {
                    case "open":
                        return new OpenExpression((AbstractExpression)tokens[0], BuildAST(SubArray(tokens, 2)));
                    case "findID":
                        return new FindIDExpression((AbstractExpression)tokens[0], BuildAST(SubArray(tokens, 2)));
                    case "input":
                        return new InputExpression((AbstractExpression)tokens[0], BuildAST(SubArray(tokens, 2)));
                    case "close":
                        return new CloseExpression((AbstractExpression)tokens[0]);
                    case "clear":
                        return new ClearExpression((AbstractExpression)tokens[0]);
                }
            }
                        
            return ALUParse(tokens);
        }
        public static AbstractExpression[] Parse (object[][] tokens)
        {
            AbstractExpression[] trees = new AbstractExpression[tokens.Count()];

            for (int i = 0; i < tokens.Count(); ++i)
                trees[i] = BuildAST(tokens[i]);
            
            return trees;
        }
    }
}
