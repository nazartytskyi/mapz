﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class InputExpression: BinaryExpression
    {
        public InputExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context), rret = rhs.Solve(context);
            if (!(lret is VariableType) && !(((VariableType)lret).Data is WebElementType))
                throw new Exception("Left hand child node of InputExpression is not a WebElement");
            WebElementType field = (WebElementType)((VariableType)lret).Data;
            if (rret is ConstantType || rret is VariableType)
                field.Element.SendKeys(rret.ToString());
            else throw new Exception("Invalid input");
            return null;
        }
    }
}
