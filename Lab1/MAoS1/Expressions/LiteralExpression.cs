﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1.Expressions
{
    class LiteralExpression: AbstractExpression
    {
        private ConstantType Literal;
        public LiteralExpression(int value) => Literal = new ConstantType(new IntType(value));
        public LiteralExpression(double value) => Literal = new ConstantType(new FloatType(value));
        public LiteralExpression(bool value) => Literal = new ConstantType(new BoolType(value));
        public LiteralExpression(string value) => Literal = new ConstantType(new StringType(value));

        public override object Solve(Context context)
        {
            return Literal;
        }
    }
}
