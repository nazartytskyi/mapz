﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class ClearExpression: UnaryExpression
    {
        public ClearExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context);
            if (!(lret is VariableType) && !(((VariableType)lret).Data is WebElementType))
                throw new Exception("Left hand child node of ClearExpression is not a WebElement");
            WebElementType field = (WebElementType)((VariableType)lret).Data;
            field.Element.Clear();
            return null;
        }
    }
}
