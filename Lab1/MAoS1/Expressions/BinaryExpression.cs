﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    abstract class BinaryExpression : UnaryExpression
    {
        public AbstractExpression rhs { get; set; }
    }
}
