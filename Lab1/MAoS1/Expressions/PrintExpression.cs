﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class PrintExpression : UnaryExpression
    {
        public PrintExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            object ret = lhs.Solve(context);
            if (ret is ConstantType || ret is VariableType)
                context.AddToOutput(ret.ToString());
            else
                throw new Exception("Invalid print parameter");
            return null;
        }
    }
}
