﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class WhileExpression: UnaryExpression
    {
        private readonly string ThenCode;

        public WhileExpression(AbstractExpression condition, string thenc)
        {
            lhs = condition;
            ThenCode = thenc;
        }

        public override object Solve(Context context)
        {
            object lret;
            DataType lvar;
            var tokens = Lexer.Tokenize(ThenCode);
            var lines = Parser.Parse(tokens);

            while (true)
            {
                lret = lhs.Solve(context);
                lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data;
                if (lvar is BoolType)
                {
                    if (((BoolType)lvar).Data)
                        foreach (var line in lines)
                            line.Solve(context);
                    else
                        break;
                }
                else
                    throw new Exception("Expected boolean value.");
            }
                       
            return null;
        }
    }
}
