﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class IfExpression: UnaryExpression
    {
        private readonly string ThenCode;
        private readonly string ElseCode;

        public IfExpression(AbstractExpression condition, string thenc, string elsec = null)
        {
            lhs = condition;
            ThenCode = thenc;
            ElseCode = elsec;
        }

        public override object Solve(Context context)
        {
            string executed;
            object lret = lhs.Solve(context);

            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data;

            if (lvar is BoolType)
            {
                if (((BoolType)lvar).Data)
                    executed = ThenCode;
                else
                    if (ElseCode == null)
                        return null;
                    else executed = ElseCode;
            }
            else
                throw new Exception("Expected boolean value.");

            var tokens = Lexer.Tokenize(executed);
            var lines = Parser.Parse(tokens);
            foreach (var line in lines)
                line.Solve(context);

            return null;
        }
    }
}
