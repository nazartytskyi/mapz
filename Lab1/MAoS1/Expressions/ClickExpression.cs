﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class ClickExpression : UnaryExpression
    {
        public ClickExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            object ret = lhs.Solve(context);
            if (!(ret is VariableType))
                throw new Exception("Invalid ClickExpression child expression");
            DataType retvar = ((VariableType)ret).Data;
            if (retvar is WebElementType)
                ((WebElementType)retvar).Element.Click();
            else
                throw new Exception("Invalid ClickExpression child expression");
            return null;
        }
    }
}
