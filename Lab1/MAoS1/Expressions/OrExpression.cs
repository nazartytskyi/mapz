﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class OrExpression: BinaryExpression
    {
        public OrExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context);
            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data;
            if (lvar is BoolType)
            {
                if (((BoolType)lvar).Data)
                    return new ConstantType(new BoolType(true));
            }
            else
                throw new Exception("Expected boolean values.");

            object rret = rhs.Solve(context);
            DataType rvar = (rret is ConstantType) ? ((ConstantType)rret).Data : ((VariableType)rret).Data;
            if (rvar is BoolType)
                return new ConstantType(new BoolType(((BoolType)rvar).Data));
            else
                throw new Exception("Expected boolean values.");
        }
    }
}
