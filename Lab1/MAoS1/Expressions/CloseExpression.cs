﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class CloseExpression: UnaryExpression
    {
        public CloseExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context);
            if (!(lret is VariableType) && !(((VariableType)lret).Data is BrowserType))
                throw new Exception("Left hand child node of CloseExpression is not a Browser");

            BrowserType browser = (BrowserType)((VariableType)lret).Data;
                browser.Driver.Quit();

            return null;
        }
    }
}
