﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class OpenExpression : BinaryExpression
    {
        public OpenExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context), rret = rhs.Solve(context);
            if (!(lret is VariableType) && !(((VariableType)lret).Data is BrowserType))
                throw new Exception("Left hand child node of OpenExpression is not a Browser");

            BrowserType browser = (BrowserType)((VariableType)lret).Data;
            if (rret is ConstantType || rret is VariableType)
                browser.Driver.Url = rret.ToString();
            else
                throw new Exception("Invalid link");

            return null;
        }
    }
}
