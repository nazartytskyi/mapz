﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class VariableExpression : AbstractExpression
    {
        public readonly string Name;
        public VariableExpression(string value) => Name = value;

        public override object Solve(Context context)
        {
            if (context.Vars.ContainsKey(Name))
                return context.Vars[Name];
            return Name;
        }
    }
}
