﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace MAoS1
{
    class FindIDExpression : BinaryExpression
    {
        public FindIDExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            object lret = lhs.Solve(context), rret = rhs.Solve(context);
            ConstantType ret;
            if (!(lret is VariableType) && !(((VariableType)lret).Data is BrowserType))
                throw new Exception("Left hand child node of FindIDExpression is not a Browser");
            BrowserType browser = (BrowserType)((VariableType)lret).Data;
            if (rret is ConstantType || rret is VariableType)
                ret = new ConstantType(new WebElementType(browser.Driver.FindElement(By.Id(rret.ToString()))));
            else throw new Exception("Invalid ID");
            return ret;
        }
    }
}
