﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class GetBrowserExpression : AbstractExpression
    {
        public override object Solve(Context context)
        {
            return new ConstantType(new BrowserType());
        }
    }
}
