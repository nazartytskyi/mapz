﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class AssignExpression : BinaryExpression
    {
        public AssignExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }

        private void CastAssign(VariableType lvar, DataType rvar)
        {
            if (lvar.Data == null)
            {
                if (rvar is StringType)
                {
                    lvar.Data = new StringType(((StringType)rvar).Data);
                }
                else if (rvar is BoolType)
                {
                    lvar.Data = new BoolType(((BoolType)rvar).Data);
                }
                else if (rvar is FloatType)
                {
                    lvar.Data = new FloatType(((FloatType)rvar).Data);
                }
                else if (rvar is IntType)
                {
                    lvar.Data = new IntType(((IntType)rvar).Data);
                }
                else
                    lvar.Data = rvar; //kinda reference type
            }
            else if (lvar.Data is IntType)
            {
                if (rvar is StringType)
                {
                    ((IntType)lvar.Data).Data = int.Parse((rvar as StringType).Data);
                }
                else if (rvar is BoolType)
                {
                    ((IntType)lvar.Data).Data = (rvar as BoolType).Data ? 1 : 0;
                }
                else if (rvar is FloatType)
                {
                    ((IntType)lvar.Data).Data = (int)(rvar as FloatType).Data;
                }
                else
                    lvar.Data = rvar;
            }
            else if (lvar.Data is FloatType)
            {
                if (rvar is StringType)
                {
                    ((FloatType)lvar.Data).Data = double.Parse((rvar as StringType).Data);
                }
                else if (rvar is BoolType)
                {
                    ((FloatType)lvar.Data).Data = (rvar as BoolType).Data ? 1 : 0;
                }
                else if (rvar is IntType)
                {
                    ((FloatType)lvar.Data).Data = (rvar as IntType).Data;
                }
                else
                    lvar.Data = rvar;
            }
            else if (lvar.Data is BoolType)
            {
                if (rvar is StringType)
                {
                    ((BoolType)lvar.Data).Data = bool.Parse((rvar as StringType).Data);
                }
                else if (rvar is FloatType)
                {
                    ((BoolType)lvar.Data).Data = Math.Abs((rvar as FloatType).Data) > Double.Epsilon;
                }
                else if (rvar is IntType)
                {
                    ((BoolType)lvar.Data).Data = (rvar as IntType).Data != 0;
                }
                else
                    lvar.Data = rvar;
            }
            else if (lvar.Data is StringType)
            {
                ((StringType)lvar.Data).Data = rvar.ToString();
            }
            else
                lvar.Data = rvar;
        }

        public override object Solve(Context context)
        {
            object lchildresult = lhs.Solve(context);
            object rchildresult = rhs.Solve(context);
            
            if (!(lchildresult is VariableType))
            {
                throw new Exception("Left child node of AssignExpression is constant");
            }
            DataType lvar = ((VariableType)lchildresult).Data;
            DataType rvar = null;
            if (rchildresult is VariableType)
            {
                rvar = ((VariableType)rchildresult).Data;
                if (lvar != null && lvar.GetType() == rvar.GetType())
                {
                    ((VariableType)lchildresult).Data = rvar.Clone();
                }
                else CastAssign((VariableType)lchildresult, rvar);
            }
            if (rchildresult is ConstantType)
            {
                rvar = ((ConstantType)rchildresult).Data;
                if (lvar != null && lvar.GetType() == rvar.GetType())
                {
                    ((VariableType)lchildresult).Data = rvar;
                }
                else
                    CastAssign((VariableType)lchildresult, rvar);
            }
            else throw new Exception("Invalid type of left child node of AssignExpression");
            return new ConstantType(lvar);
        }
    }
}
