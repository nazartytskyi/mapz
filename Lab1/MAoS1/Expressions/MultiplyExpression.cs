﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class MultiplicationExpression: BinaryExpression
    {
        public MultiplicationExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            ConstantType mult = null;
            object lret = lhs.Solve(context),
                   rret = rhs.Solve(context);

            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data,
                         rvar = (rret is ConstantType) ? ((ConstantType)rret).Data : ((VariableType)rret).Data;

            if (lvar.GetType() == rvar.GetType())
            {
                if (lvar.GetType() == typeof(IntType))
                    mult = new ConstantType(new IntType(((IntType)lvar).Data * ((IntType)rvar).Data));
                else if (lvar.GetType() == typeof(FloatType))
                    mult = new ConstantType(new FloatType(((FloatType)lvar).Data * ((FloatType)rvar).Data));
            }
            else if (lvar.GetType() == typeof(IntType) && rvar.GetType() == typeof(FloatType))
                mult = new ConstantType(new FloatType(((IntType)lvar).Data * ((FloatType)rvar).Data));
            else if (lvar.GetType() == typeof(FloatType) && rvar.GetType() == typeof(IntType))
                mult = new ConstantType(new FloatType(((FloatType)lvar).Data * ((IntType)rvar).Data));
            else
                throw new Exception("Only numeric types can be multiplied.");

            return mult;
        }
    }
}
