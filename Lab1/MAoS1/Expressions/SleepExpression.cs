﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class SleepExpression: UnaryExpression
    {
        public SleepExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            object ret = lhs.Solve(context);

            DataType var = (ret is ConstantType) ? ((ConstantType)ret).Data : ((VariableType)ret).Data;

            if (var is IntType)
                System.Threading.Thread.Sleep(((IntType)var).Data);
            else
                throw new Exception("Invalid sleep parameter.");
            return null;
        }
    }
}
