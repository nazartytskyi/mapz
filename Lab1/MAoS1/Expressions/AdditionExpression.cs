﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class AdditionExpression : BinaryExpression
    {
        public AdditionExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            ConstantType summ = null;
            object lret = lhs.Solve(context), 
                   rret = rhs.Solve(context);

            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data,
                         rvar = (rret is ConstantType) ? ((ConstantType)rret).Data : ((VariableType)rret).Data;

            if (lvar.GetType() == rvar.GetType())
            {
                if (lvar.GetType() == typeof(IntType))
                    summ = new ConstantType(new IntType(((IntType)lvar).Data + ((IntType)rvar).Data));
                else if (lvar.GetType() == typeof(FloatType))
                    summ = new ConstantType(new FloatType(((FloatType)lvar).Data + ((FloatType)rvar).Data));
                else if (lvar.GetType() == typeof(StringType))
                    summ = new ConstantType(new StringType(((StringType)lvar).Data + ((StringType)rvar).Data));
            }
            else if (lvar.GetType() == typeof(IntType) && rvar.GetType() == typeof(FloatType))
                summ = new ConstantType(new FloatType(((IntType)lvar).Data + ((FloatType)rvar).Data));
            else if (lvar.GetType() == typeof(FloatType) && rvar.GetType() == typeof(IntType))
                summ = new ConstantType(new FloatType(((FloatType)lvar).Data + ((IntType)rvar).Data));
            else
                summ = new ConstantType(new StringType(lvar.ToString() + rvar.ToString()));

            return summ;
        }
    }
}
