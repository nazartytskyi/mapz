﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class NotExpression: UnaryExpression
    {
        public NotExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            ConstantType res = null;
            object lret = lhs.Solve(context);

            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data;

            if (lvar is BoolType)
            {
                res = new ConstantType(new BoolType(!((BoolType)lvar).Data));
            }
            else
                throw new Exception("Expected boolean value.");

            return res;
        }
    }
}
