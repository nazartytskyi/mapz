﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    abstract class AbstractExpression
    {
        abstract public Object Solve(Context context);
        
    }
}
