﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class PowerExpression: BinaryExpression
    {
        public PowerExpression(AbstractExpression lhs, AbstractExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = rhs;
        }
        public override object Solve(Context context)
        {
            ConstantType power = null;
            object lret = lhs.Solve(context),
                   rret = rhs.Solve(context);

            DataType lvar = (lret is ConstantType) ? ((ConstantType)lret).Data : ((VariableType)lret).Data,
                         rvar = (rret is ConstantType) ? ((ConstantType)rret).Data : ((VariableType)rret).Data;

            if (lvar.GetType() == rvar.GetType())
            {
                if (lvar.GetType() == typeof(IntType))
                    power = new ConstantType(new FloatType(Math.Pow(((IntType)lvar).Data, ((IntType)rvar).Data)));
                else if (lvar.GetType() == typeof(FloatType))
                    power = new ConstantType(new FloatType(Math.Pow(((FloatType)lvar).Data, ((FloatType)rvar).Data)));
            }
            else if (lvar.GetType() == typeof(IntType) && rvar.GetType() == typeof(FloatType))
                power = new ConstantType(new FloatType(Math.Pow(((IntType)lvar).Data, ((FloatType)rvar).Data)));
            else if (lvar.GetType() == typeof(FloatType) && rvar.GetType() == typeof(IntType))
                power = new ConstantType(new FloatType(Math.Pow(((FloatType)lvar).Data, ((IntType)rvar).Data)));
            else
                throw new Exception("Only numeric types can be powered.");

            return power;
        }
    }
}
