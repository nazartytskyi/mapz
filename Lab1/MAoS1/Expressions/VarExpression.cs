﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class VarExpression: UnaryExpression
    {
        public VarExpression(AbstractExpression lhs)
        {
            this.lhs = lhs;
        }
        public override object Solve(Context context)
        {
            if (!(lhs is VariableExpression))
                throw new Exception("Invalid variable name");

            object ret = lhs.Solve(context);
            VariableType returned = null;
            if (ret is VariableType)
                throw new Exception("Variable already exists");
            if (ret is string)
            {
                string varname = (string)ret;
                if (context.Vars.ContainsKey(varname))
                    throw new Exception("Variable already exists");
                else
                {
                    context.Vars.Add(varname, new VariableType(null));
                    returned = context.Vars[varname];
                }
            }
            else throw new Exception("Invalid child expression of IntExpression instance");
            return returned;
        }
    }
}
