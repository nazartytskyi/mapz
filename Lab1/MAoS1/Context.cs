﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;

namespace MAoS1
{
    class Context
    {
        public Dictionary<string, VariableType> Vars { get; }
        private string _output;
        public string Output
        {
            get
            {
                string ret = _output;
                _output = "";
                return ret;
            }
            
        }
        
        public Context() => Vars = new Dictionary<string, VariableType>();
        public void AddToOutput(string str) => _output += str;

    }
}
