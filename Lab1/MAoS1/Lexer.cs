﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAoS1
{
    static class Lexer
    {

        static readonly string[] keywords = {"var", "if", "while", "else", "open", "print",
            "sleep", "close", "findID", "GetBrowser", "click", "input", "clear"};
        static readonly string[] doperators = { "<=", ">=", "==", "!=" };
        static readonly string[] operators = { "+", "-", "*", "/", "^", "!", "&", "|", "<", ">", "=", "(", ")" };
       
        private static bool IsBool(string src)
        {
            return (src == "true" || src == "false");
        }

        public static object[][] Tokenize (string src)
        {
            List<List<object>> tokens = new List<List<object>>
            {
                new List<object>()
            };

            StringBuilder num = new StringBuilder(),
                          tok = new StringBuilder();

            bool isFloat = false, isStr = false, inBracket = false;
            int bracket = 0;
            int condpos = -1;
            int row = 0;

            for (int i = 0; i < src.Length; ++i)
            {
                if (inBracket)
                {
                    if (src[i] == '}')
                    {
                        if (bracket > 0)
                            bracket--;
                        else
                        {
                            inBracket = false;
                            tokens[condpos].Add(tok.ToString());
                            tok.Clear();
                            continue;
                        }
                    }
                    if (src[i] == '{')
                        bracket++;
                    tok.Append(src[i]);
                    continue;
                }

                if (isStr)
                {
                    if (src[i] == '"')
                    {
                        if (src[i - 1] == '\\')
                        {
                            tok.Append('"');
                            continue;
                        }
                        else
                        {
                            isStr = false;
                            tokens.Last().Add(new ConstantType(new StringType(tok.ToString())));
                            tok.Clear();
                            continue;
                        }
                    }
                    else
                    {
                        tok.Append(src[i]);
                        continue;
                    }
                }

                if (Char.IsNumber(src[i]))
                {
                    if (tok.ToString() != "")
                    {
                        tok.Append(src[i]);
                        continue;
                    }
                    else
                    {
                        num.Append(src[i]);
                        continue;
                    }
                }

                if (Char.IsLetter(src[i]) || src[i] == '_')
                {
                    if (num.ToString() != "")
                        throw new Exception($"Lexical error on line {tokens.Count}. Invalid variable name.");
                    tok.Append(src[i]);
                    continue;
                }

                if (src[i] == '.')
                {
                    if (isFloat)
                        throw new Exception($"Lexical error on line {tokens.Count}. Invalid float number.");
                    else
                    {
                        isFloat = true;
                        num.Append('.');
                        continue;
                    }
                }

                if (num.ToString() != "")
                {
                    if (isFloat)
                    {
                        isFloat = false;
                        tokens.Last().Add(new ConstantType(new FloatType(double.Parse(num.ToString()))));
                    }
                    else
                        tokens.Last().Add(new ConstantType(new IntType(int.Parse(num.ToString()))));
                    num.Clear();
                }

                if (tok.ToString() != "")
                {
                    string stok = tok.ToString();
                    if (IsBool(stok))
                        tokens.Last().Add(new ConstantType(new BoolType(stok == "true")));
                    else if (keywords.Contains(stok))
                    {
                        if (stok == "while" || stok == "if")
                            condpos = row;
                        if (stok == "else")
                            tokens[condpos].Add(stok);
                        else
                            tokens.Last().Add(stok);
                    }
                    else
                        tokens.Last().Add(new VariableExpression(stok));
                    tok.Clear();
                }

                if (src[i] == '-' && tokens.Last().Last() is string)
                {
                    num.Append("-");
                    continue;
                }

                if (src[i] == '"')
                {
                    isStr = true;
                    continue;
                }

                if (src[i] == '{')
                {
                    inBracket = true;
                    continue;
                }

                // handling operators which consist of 2 characters
                if (i < src.Length - 1)
                {
                    string dop = new string(new char[]{ src[i], src[i + 1]});
                    if (doperators.Contains(dop))
                    {
                        tokens.Last().Add(dop);
                        i++;
                        continue;
                    }
                }

                // handling operators which consist of 1 character1
                if (operators.Contains(new string(src[i], 1)))
                {
                    tokens.Last().Add(new string(src[i], 1));
                    continue;
                }

                if (src[i] == '\n' || src[i] == '\r')
                {
                    tokens.Add(new List<object>());
                    ++row;
                    continue;
                }

                if (src[i] != ' ' && src[i] != '\t')
                    throw new Exception($"Lexical error on line {tokens.Count}. Invalid character '{src[i]}'.");
            }

            if (num.ToString() != "")
            {
                if (isFloat)
                {
                    isFloat = false;
                    tokens.Last().Add(new ConstantType(new FloatType(double.Parse(num.ToString()))));
                }
                else
                    tokens.Last().Add(new ConstantType(new IntType(int.Parse(num.ToString()))));
                num.Clear();
            }

            if (tok.ToString() != "")
            {
                if (IsBool(tok.ToString()))
                    tokens.Last().Add(new ConstantType(new BoolType(tok.ToString() == "true")));
                else if (keywords.Contains(tok.ToString()))
                    tokens.Last().Add(tok.ToString());
                else
                    tokens.Last().Add(new VariableExpression(tok.ToString()));
                tok.Clear();
            }

            for (int i = 0; i < tokens.Count; ++i)
                if (tokens[i].Count < 1)
                {
                    tokens.RemoveAt(i);
                    --i;
                }

            object[][] toks = new object[tokens.Count][];
            for (int i = 0; i < tokens.Count; ++i)
                toks[i] = tokens[i].ToArray();
            return toks;
        }

    }
}
