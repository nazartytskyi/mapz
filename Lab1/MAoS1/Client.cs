﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MAoS1
{
    class Client
    {
        public TreeNode[] ExpressionTree
        {
            get
            {
                return exprtree;
            }
        }

        private TreeNode[] exprtree;
        private string source;
        private TextBox output;
        private bool optimize;
        private Context context;

        public Client(TextBox outputBox)
        {
            output = outputBox;
        }

        public string GetOutput()
        {
            return context.Output;
        }

        private TreeNode BuildNode(AbstractExpression expr)
        {
            TreeNode node;
            if (expr is ConstantType cexpr)
                node = new TreeNode(cexpr.Data.ToString());
            else if (expr is VariableExpression vexpr)
                node = new TreeNode("$" + vexpr.Name);
            else
                node = new TreeNode(expr.GetType().ToString());

            if (expr is BinaryExpression bexpr)
            {
                node.Nodes.Add(BuildNode(bexpr.lhs));
                node.Nodes.Add(BuildNode(bexpr.rhs));
            }
            else if (expr is UnaryExpression uexpr)
            {
                node.Nodes.Add(BuildNode(uexpr.lhs));
            }
            return node;
        }
        
        private void Run()
        {
            context = new Context();
            try
            {
                var tokens = Lexer.Tokenize(source);
                var lines = Parser.Parse(tokens);
                if (optimize)
                    lines = Optimizer.Optimize(lines);
                
                TreeNode[] tns = new TreeNode[lines.Count()];
                for (int i = 0; i < lines.Count(); ++i)
                    tns[i] = BuildNode(lines[i]);

                exprtree = tns;

                for (int i = 0; i < lines.Count(); ++i)
                {
                    try
                    {
                        lines[i].Solve(context);

                        output.Invoke((MethodInvoker)delegate {
                            output.Text += context.Output + "\n";
                        });
                    }
                    catch (Exception ex)
                    {
                        context.AddToOutput($"An error occured on line {i + 1}.\n\tException:\n{ex.Message}\n");
                    }
                }
            }
            catch (Exception ex)
            {
                context.AddToOutput($"An error occured.\n\tException:\n{ex.Message}\n");
            }
        }

        public void Execute(string src, bool optimize)
        {
            source = src;
            this.optimize = optimize;
            exprtree = null;
            ThreadStart thrdel = new ThreadStart(Run);
            Thread thread = new Thread(thrdel);
            thread.Start();
        }
    }
}
