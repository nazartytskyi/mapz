﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace MAoS1
{
    static class Optimizer
    {
        private static Dictionary<string, AbstractExpression> vars;
        private static bool CheckForAssigns(AbstractExpression node)
        {
            if (node is AssignExpression)
                return true;

            if (node is BinaryExpression bnode)
                return (CheckForAssigns(bnode.lhs) || CheckForAssigns(bnode.rhs));
            if (node is UnaryExpression unode)
                return CheckForAssigns(unode.lhs);
            return false; 
        }

        private static void ReplaceVars(AbstractExpression baseNode)
        {
            if (baseNode is BinaryExpression bnode)
            {
                if (bnode.lhs is VariableExpression lvarnode)
                {
                    if (vars.ContainsKey(lvarnode.Name))
                        bnode.lhs = vars[lvarnode.Name];
                }
                else
                    ReplaceVars(bnode.lhs);

                if (bnode.rhs is VariableExpression rvarnode)
                {
                    if (vars.ContainsKey(rvarnode.Name))
                        bnode.rhs = vars[rvarnode.Name];
                }
                else
                    ReplaceVars(bnode.rhs);
            }
            else if (baseNode is UnaryExpression unode)
            {
                if (unode.lhs is VariableExpression lvarnode)
                {
                    if (vars.ContainsKey(lvarnode.Name))
                        unode.lhs = vars[lvarnode.Name];
                }
                else
                    ReplaceVars(unode.lhs);
            }
        }

        public static AbstractExpression[] Optimize (AbstractExpression[] lines)
        {
            vars = new Dictionary<string, AbstractExpression>();
            List<AbstractExpression> newlines = new List<AbstractExpression>();
            foreach(var line in lines)
            {
                if (line is AssignExpression asnode)
                {
                    if (asnode.rhs is VariableExpression avarnode)
                    {
                        if (vars.ContainsKey(avarnode.Name))
                        {
                            asnode.rhs = vars[avarnode.Name];
                            continue;
                        }
                    }
                    else
                        ReplaceVars(asnode.rhs);

                    if (!CheckForAssigns(asnode.rhs) && !(asnode.rhs is GetBrowserExpression) && !(asnode.rhs is FindIDExpression))
                    {
                        if (asnode.lhs is VariableExpression varnode)
                        {
                            if (vars.ContainsKey(varnode.Name))
                                vars[varnode.Name] = asnode.rhs;
                            else
                                vars.Add(varnode.Name, asnode.rhs);
                            continue;
                        }
                        else if ((asnode.lhs is VarExpression vnode) && (vnode.lhs is VariableExpression vvarnode))
                        {
                            if (vars.ContainsKey(vvarnode.Name))
                                vars[vvarnode.Name] = asnode.rhs;
                            else
                                vars.Add(vvarnode.Name, asnode.rhs);
                            continue;
                        }
                    }
                }
                else
                    ReplaceVars(line);
                newlines.Add(line);
            }
            return newlines.ToArray();
        }
    }
}
