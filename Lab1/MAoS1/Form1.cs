﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAoS1
{
    public partial class Form1 : Form
    {
        Client Cl;
        public Form1()
        {
            InitializeComponent();
            Cl = new Client(textBoxOutput);
        }


        private void ButtonRun_Click(object sender, EventArgs e)
        {
            textBoxOutput.Text = "";
            Cl.Execute(textBoxSource.Text, checkBoxOptimize.Checked);
            
            while (Cl.ExpressionTree == null) ;

            expressionTree.BeginUpdate();
            expressionTree.Nodes.Clear();
            foreach (var node in Cl.ExpressionTree)
                expressionTree.Nodes.Add(node);
            expressionTree.EndUpdate();
        }
    }
}
