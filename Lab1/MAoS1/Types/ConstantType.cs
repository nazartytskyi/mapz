﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class ConstantType: AbstractExpression
    {
        public ConstantType(DataType value) => _data = value;

        private DataType _data;
        public DataType Data
        {
            get
            {
                return _data.Clone();
            }
        }

        public override object Solve(Context context)
        {
            return this;
        }
        
        public override string ToString()
        {
            return _data.ToString();
        }
    }
}
