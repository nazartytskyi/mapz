﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace MAoS1
{
    class WebElementType : DataType
    {
        public IWebElement Element { get; set; }
        public WebElementType(IWebElement element) => Element = element;
        public override DataType Clone()
        {
            return this;
        }

        public override string ToString()
        {
            return "Web element entity";
        }
    }
}
