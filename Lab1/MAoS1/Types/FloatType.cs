﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class FloatType: DataType
    {
        public FloatType(double value = 0) => Data = value;

        public double Data { get; set; }

        public override DataType Clone()
        {
            return new FloatType(Data);
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
