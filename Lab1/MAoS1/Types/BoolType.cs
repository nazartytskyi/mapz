﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class BoolType: DataType
    {
        public BoolType(bool value = false) => Data = value;

        public bool Data { get; set; }

        public override DataType Clone()
        {
            return new BoolType(Data);
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
