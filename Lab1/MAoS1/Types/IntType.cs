﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class IntType: DataType 
    {
        public IntType(int value = 0) => Data = value;
        
        public int Data { get; set; }

        public override DataType Clone()
        {
            return new IntType(Data);
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
