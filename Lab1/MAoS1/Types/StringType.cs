﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class StringType: DataType
    {
        public StringType(string value = "") => Data = (string) value.Clone();

        public string Data { get; set; }

        public override DataType Clone()
        {
            return new StringType(Data);
        }

        public override string ToString()
        {
            return Data;
        }
    }
}
