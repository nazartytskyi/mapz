﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;

namespace MAoS1
{
    class BrowserType: DataType
    {
        public IWebDriver Driver { get; }
  
        private readonly static string ChromeLocation = @".";
        public BrowserType() => Driver = new ChromeDriver(ChromeLocation);

        public override DataType Clone()
        {
            return this;
        }

        public override string ToString()
        {
            return "Google Chrome Browser";
        }
    }
}
