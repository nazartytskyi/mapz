﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAoS1
{
    class VariableType
    {
        public VariableType(DataType value) => Data = value;

        public DataType Data { get; set; }
    
            
        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
