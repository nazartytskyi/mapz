﻿namespace MAoS1
{
    abstract class DataType
    {
        public abstract DataType Clone();
        public abstract override string ToString();
    }
}
